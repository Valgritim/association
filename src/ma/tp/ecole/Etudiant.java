package ma.tp.ecole;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Etudiant {

	private int id;
	private String nom;
	private String prenom;
	private static int count;
	private Date date;
	private Filiere filiere;
	
	public Etudiant(String nom, String prenom, Date date, Filiere filiere) {
		super();
		this.id = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.date = date;
		this.filiere = filiere;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Filiere getFiliere() {
		return filiere;
	}



	@Override
	public String toString() {
		SimpleDateFormat df = new SimpleDateFormat("d MMMM yyyy");
		String date = df.format(this.getDate());
		return "Etudiant [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", date=" + date + ", filiere=" + filiere
				+ "]";
	}
	

}
