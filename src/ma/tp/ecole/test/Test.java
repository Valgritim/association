package ma.tp.ecole.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ma.tp.ecole.Etudiant;
import ma.tp.ecole.Filiere;

public class Test {

	public static void main(String[] args) {
		
		Filiere [] filieres = new Filiere [3];
		filieres[0] = new Filiere("MA","Mathématiques");
		filieres[1] = new Filiere("PHY", "Astrophysique");
		filieres[2] = new Filiere("INF", "Informatique");
		
		List<Etudiant> etudiants = new ArrayList<Etudiant>();
		
		etudiants.add(new Etudiant("Gates", "Bill", new Date("03/25/1980"), filieres[2]));
		etudiants.add(new Etudiant("Einstein", "Albert", new Date("03/25/1980"), filieres[0]));
		etudiants.add(new Etudiant("Reeves", "Hubert", new Date("03/25/1980"), filieres[1]));		
		etudiants.add(new Etudiant("Zuckerberg", "Mark", new Date("03/25/1980"), filieres[2]));
		etudiants.add(new Etudiant("Klein", "Etienne", new Date("03/25/1980"), filieres[1]));
		
		
		for(Filiere filiere:filieres) {
			for(Etudiant etudiant: etudiants) {
				if(etudiant.getFiliere().getId() == filiere.getId()) {
					System.out.println(etudiant);
				}
			}
		}
			

	}

}
